from django.db import models
from django import forms

class Customer(models.Model):
    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length = 30)
    parser_choise = (
        ('A', 'parser 1'),
        ('B', 'parser 2'),
        ('C', 'parcer 3'),
        ('C', 'parcer 4'),
    )
    parser = models.CharField(max_length = 1, choices=parser_choise)

    def __str__(self):
        return self.name




class FileParser(models.Model):
    id = models.AutoField(primary_key = True)
    customer = models.ForeignKey(Customer , on_delete=models.CASCADE)
    email_id = models.IntegerField(default=0)
    email_date  =  models.DateTimeField()
    #list_parser
    option_uploand = (
                     ('byEmail' , 'Email'),
                     ('uploand_by_web' , 'Web'),
                     )

    send = (
        ('True' , 'true'),
        ('False' , 'false'),
    )
    option = models.CharField(max_length = 30 , choices = option_uploand)
    sended  = models.CharField(max_length = 30 , choices = send)
    file_name = models.FileField(upload_to= 'uploads/')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
       return self.customer.name

    def get_info(self):
        return 'id {} , customer {} , email_id {} , email_date {} ,  option {}, sended {}'.format(self.id , self.customer ,self.email_id , self.email_date , self.option ,self.sended )


