from django.http import HttpResponse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView , DetailView
from App.models import Customer , FileParser
from App.form import CustomerForm , FileParserForm , FilterForm
from django.views import View
from django.shortcuts import render
from django.urls import reverse_lazy
from .export_process import *
from .tasks import export_file , sendmail
from django.views.generic import TemplateView

class HomeView(TemplateView):
    template_name = "App/index.html"

class CreateCustom(CreateView):
    model = Customer
    form_class = CustomerForm
    template_name = 'App/'
    success_url = reverse_lazy('homeview')


class CreateFileParser(CreateView):
    model = FileParser
    form_class = FileParserForm
    template_name = 'App/'
    success_url = reverse_lazy('create_file')


class FilterView(View):

    def __init__(self):
        self.template_name = 'App/filter.html'
        self.customers = Customer.objects.all()


    def get(self ,request):
        return render(request, self.template_name ,{'customers':self.customers})

    def post(self ,request):
        info_request= {'filter_by':request.POST['filter_by'] , 'customer_id':request.POST['customer'],
                       'initial_date':request.POST['initial_date'] ,'final_date': request.POST['final_date'],
                       'status':request.POST['status']}

        task1 = export_file.delay(info_request)
        task2 = sendmail.delay()
        return HttpResponse(task1)

class Index(View):

    def __init__(self):
        self.template_name = 'App/index.html'
        self.customers = Customer.objects.all()


    def get(self ,request):
        return render(request, self.template_name ,{'customers':self.customers})

    def post(self ,request):
        print(request.POST)
        info_request= {'filter_by':request.POST['filter_by'] , 'customer_id':request.POST['customer'],
                       'initial_date':request.POST['initial_date'] ,'final_date': request.POST['final_date'],
                       'status':request.POST['status']}

        task1 = export_file.delay(info_request)
        task2 = sendmail.delay()
        return HttpResponse(task1)








