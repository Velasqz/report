from django import forms
from App.models import Customer , FileParser

class CustomerForm(forms.ModelForm):
   class Meta:
        model = Customer

        fields = [
            'name',
            'parser',
        ]

        labels = {
            'name':'Nombre',
            'parser':'Parser',
        }

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'parser': forms.Select(attrs={'class': 'form-control'}),
        }


class FileParserForm(forms.ModelForm):
    class Meta:

        model = FileParser
        fields = [
            'customer',
            'email_id',
            'email_date',
            'option',
            'sended',
            'file_name',
        ]

        labels = {
            'customer':'Customer',
            'email_id':'Email id',
            'email_date':'Email Date',
            'option':'Option',
            'sended':'Sended',
            'file_name':'File',
        }

        widgets = {
            'customer': forms.Select(attrs={'class': 'form-control'}),
            'email_id': forms.TextInput(attrs={'class': 'form-control'}),
            'email_date' : forms.DateTimeInput(attrs={'class': 'form-control','type':'date'}),
            'option': forms.Select(attrs={'class': 'form-control'}),
            'sended': forms.Select(attrs={'class': 'form-control'}),
            'file_name' :forms.ClearableFileInput(attrs={'multiple': True})
        }


class FilterForm(forms.Form):
    customer = forms.ModelMultipleChoiceField(queryset=Customer.objects.all())
    filter = [
        ('email', 'email'),
        ('date', 'date'),
    ]

    status_send = [
        ('send', 'send'),
        ('no send', 'no send'),
    ]
    filter_by = forms.MultipleChoiceField(required=False,widget=forms.CheckboxSelectMultiple,choices=filter)
    initial_date = forms.DateTimeField()
    finish_date = forms.DateTimeField()
    status =  forms.MultipleChoiceField(required=False,widget=forms.CheckboxSelectMultiple,choices=status_send)


