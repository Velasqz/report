from report.celery import app
from .export_process import *
import random as ra
from django.core.mail import send_mail

@app.task()
def export_file(information):
    file = Export('reporte_{}.xlsx'.format(ra.randint(1,30)))
    fpcc = file.filter(**information)
    file.field(customer = 'customer' , email_id = 'email id' ,option='option', filename='filename', email_date='email date',
           create_ad='created at')
    file.value_field(fpcc)
    return 'success'

@app.task()
def sendmail():
    send_mail(
        'Report info',
        'report has been create succefully',
        'javiergmorenovelasquez@gmail.com',
        ['javierelcacha@hotmail.com'],
        fail_silently=False,
    )
    return 'Email sended'
