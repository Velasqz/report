from django.contrib import admin

from App.models import Customer , FileParser

admin.site.register(Customer)
admin.site.register(FileParser)