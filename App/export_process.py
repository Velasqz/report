from openpyxl import Workbook
from App.models import FileParser

class Export():
    def __init__(self ,name):
        self.name =  name
        self.wb = Workbook()
        self.ws = self.wb.active

    def field(self , *args , **kwargs):
        list_value = []
        for key , value in kwargs.items():
            list_value.append(key)
        self.ws.append(list_value)


    def value_field(self ,objects):
        for object in objects:
            name = object.customer
            email = object.email_id
            email_date = object.email_date
            option = object.option
            filename = object.file_name
            create = object.created_at
            self.ws.append([str(name), str(email), str(option), str(filename), str(email_date), str(create)])
            self.wb.save(str(self.name))
        return None

    def filter(self ,*args , **kwargs):

        fpcc = ''
        if kwargs['filter_by'] == 'email':
            # file parser customer email
            fpcc = FileParser.objects.filter(customer=kwargs['customer_id'],
                                             email_date__gte=kwargs['initial_date'],
                                             email_date__lte=kwargs['final_date'],
                                             sended=kwargs['status'])

        if kwargs['filter_by'] == 'date':
            # file parser customer created_at
            fpcc = FileParser.objects.filter(customer=kwargs['customer_id'],
                                             created_at__gte=kwargs['initial_date'],
                                             created_at__lte=kwargs['final_date'],
                                             sended=kwargs['status'])
        return fpcc
